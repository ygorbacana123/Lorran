console.log("Conectando...");
const Discord = require('discord.js');
const moment = require('moment');
const config = require('./config.json');

const client = new Discord.Client()
client.prefix = config.prefix;

client.on("message", async message => {
    if(message.author.bot) return;
    if(message.content.startsWith(`<@!${client.user.id}>`) || message.content.startsWith(`<@${client.user.id}>`)){
        return message.reply("Olá!`")}
    if(!message.content.startsWith(config.prefix)) return;

let args = message.content.split(" ").slice(1);
let command = message.content.split(" ")[0];
command = command.slice(config.prefix.length);
  try {
      let commandFile = require(`./commands/${command}.js`);
      delete require.cache[require.resolve(`./commands/${command}.js`)];
      return commandFile.run(client, message, args);
  } catch (err) {
        console.error("Erro ao tentar executar este comando: " + err)
  }
})

client.on("ready", () => {
    client.user.setGame('Acabei de acordar...')
	console.log(`Bot inicializado com sucesso! Com ${client.users.size} usuários, em ${client.guilds.size} servidores!`);
	setInterval(async ()=> {
	let textList = [`${client.guilds.size} Servidores`,`Versão 1.0.2-Alpha | Recebendo atualizações todos os dias`]
	var text = textList[Math.floor(Math.random() * textList.length)];
	client.user.setActivity(text , {
	   type: 'STREAMING'
	})}, 25000)
});

client.on("guildCreate", guild => {
  console.log(`Me adicionaram no servidor: ${guild.name} (id: ${guild.id}). Existe ${guild.memberCount} membros nesse servidor!`);
});

client.on("guildDelete", guild => {
  console.log(`Fui removido do Servidor: ${guild.name} (id: ${guild.id})`);
});

client.login(config.token)