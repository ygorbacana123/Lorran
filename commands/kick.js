const Discord = require('discord.js')

module.exports.run = async (client, message, args) => {
    if(!message.member.hasPermission('KICK_MEMBERS')) return message.reply("Você não tem permissão para executar esse comando!")
    let member = message.mentions.members.first()
    if(!member)
      return message.reply("Mencione um usuário existente!")
    if(!member.kickable)
      return message.reply("Eu não posso expulsar esse usuário, tente me colocar no mais alto da hierarquia desse servidor")
    let reason = args.slice(1).join(' ')
    if(!reason) reason = "Nenhuma razão foi fornecida"
    await member.kick(reason)
      .catch(error => message.reply(`Desculpe ${message.author}, não consegui kickar o membro devido o: ${error}`))

      message.channel.send(`${message.author}`)

      let pEmbed = new Discord.RichEmbed()
          .setTitle(":hammer: | **Expulsão**")
          .addField(":thinking: | Expulso: ", `${member.user.tag}`)
          .addField(":oncoming_police_car: | Por: ", `${message.author.tag}`)
          .addField(":exclamation: | Motivo: ", `${reason}`)
          .setFooter(`${message.author.tag}`, message.author.displayAvatarURL)
          .setColor("DARK_RED").setTimestamp()

          message.channel.send(pEmbed)
          
}

module.exports.help = {
    name: "kick"
}
